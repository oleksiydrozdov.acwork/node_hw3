module.exports.characteristicsByType = (type) => {
  const characteristics = {
    sprinter: {
      dimensions: {
        width: 250,
        length: 300,
        height: 170,
      },
      payload: 1700,
    },
    smallStraight: {
      dimensions: {
        width: 250,
        length: 500,
        height: 170,
      },
      payload: 2500,
    },
    largeStraight: {
      dimensions: {
        width: 350,
        length: 700,
        height: 200,
      },
      payload: 4000,
    },
  };
  let dimensions;
  let payload;
  switch (type) {
    case "SPRINTER":
      dimensions = characteristics.sprinter.dimensions;
      payload = characteristics.sprinter.payload;
      break;
    case "SMALL STRAIGHT":
      dimensions = characteristics.smallStraight.dimensions;
      payload = characteristics.smallStraight.payload;
      break;
    case "LARGE STRAIGHT":
      dimensions = characteristics.largeStraight.dimensions;
      payload = characteristics.largeStraight.payload;
      break;
  }
  return { dimensions, payload };
};
