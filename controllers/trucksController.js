const { Truck } = require("../models/truckModel");
const { characteristicsByType } = require("./helpers");

module.exports.getTrucks = async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({ message: "Available only for driver" });
  }
  const trucks = await Truck.find({ created_by: req.user._id });
  res.json({ trucks });
};

module.exports.addTruck = async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({ message: "Available only for driver" });
  }
  const type = req.body.type;
  if (!type) {
    return res.status(400).json({ message: "No type specified" });
  }
  const { dimensions, payload } = characteristicsByType(type);
  const truck = new Truck({
    created_by: req.user._id,
    type,
    dimensions,
    payload,
  });
  await truck.save();
  res.json({ message: "Truck created successfully" });
};

module.exports.getTruck = async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({ message: "Available only for driver" });
  }
  const truck = await Truck.findById(req.params.id);
  res.json({ truck });
};

module.exports.updateTruck = async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({ message: "Available only for driver" });
  }
  const truck = await Truck.findById(req.params.id);
  if (!truck || truck.created_by !== req.user._id) {
    return res.status(400).json({ message: "You have no truck with such id" });
  }
  if (truck.assigned_to !== "unassigned") {
    return res
      .status(400)
      .json({ message: "You can not update assigned truck" });
  }
  const type = req.body.type;
  if (!type) {
    return res.status(400).json({ message: "No type specified" });
  }
  const { dimensions, payload } = characteristicsByType(type);
  truck.type = type;
  truck.dimensions = dimensions;
  truck.payload = payload;
  await Truck.findByIdAndUpdate(req.params.id, { type });
  await truck.save();
  res.json({ message: "Truck updated successfully" });
};

module.exports.deleteTruck = async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({ message: "Available only for driver" });
  }
  const truck = await Truck.findById(req.params.id);
  if (!truck || truck.created_by !== req.user._id) {
    return res.status(400).json({ message: "You have no truck with such id" });
  }
  if (truck.assigned_to !== "unassigned") {
    return res
      .status(400)
      .json({ message: "You can not delete assigned truck" });
  }
  await Truck.findByIdAndDelete(req.params.id);
  res.json({ message: "Truck deleted successfully" });
};

module.exports.assignTruck = async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({ message: "Available only for driver" });
  }

  const truck = await Truck.findById(req.params.id);
  if (!truck || truck.created_by !== req.user._id) {
    return res.status(400).json({ message: "You have no truck with such id" });
  }

  await Truck.updateMany(
    { created_by: req.user._id },
    { assigned_to: "unassigned" }
  );
  await Truck.findByIdAndUpdate(req.params.id, { assigned_to: req.user._id });
  res.json({ message: "Truck assigned successfully" });
};
