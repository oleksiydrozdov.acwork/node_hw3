const getElem = (selector) => document.querySelector(selector);

const getMainPage = async () => {
  let token = localStorage.getItem("token");
  if (token) {
    await fetch("/", {
      headers: {
        Authorization: token,
      },
    });
  }
};
setTimeout(getMainPage, 1000) ;

async function register() {
  const username = getElem('#registerUserName').value;
  const password = getElem('#registerPassword').value;
  if (!username || !password) {
    getElem('.register-message').innerText = 'Please, enter username and password';
    getElem('.register-message').classList.remove('hidden');
  } else {
    const response = await fetch('/api/auth/register', {
      method: 'POST',
      body: JSON.stringify({
        username,
        password,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.status === 200) {
      location.reload();
    }
  }
};

async function login() {
  const username = getElem('#loginUserName').value;
  const password = getElem('#loginPassword').value;
  if (!username || !password) {
    getElem('.login-message').innerText = 'Please, enter username and password';
    getElem('.login-message').classList.remove('hidden');
  } else {
    const response = await fetch('/api/auth/login', {
      method: 'POST',
      body: JSON.stringify({
        username,
        password,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.status === 200) {
      const data = await response.json();
      localStorage.setItem('token', `JWT ${data.jwt_token}`);
      location.reload();
    }
  }
}

function reset() {
  getElem('#registerForm').reset();
  getElem('#loginForm').reset();
  getElem('.login-message').classList.add('hidden');
  getElem('.register-message').classList.add('hidden');
  console.log('reset');
}
