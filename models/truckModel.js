const mongoose = require("mongoose");

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: "unassigned",
  },
  type: {
    type: String,
    required: true,
  },
  dimensions: {
    type: {
      width: Number,
      length: Number,
      height: Number,
    },
    required: true
  },
  payload: {
    type: Number,
    required: true
  },
  status: {
    type: String,
    required: true,
    default: "IS",
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Truck = mongoose.model("Truck", truckSchema);
