const express = require('express');
const { asyncWrapper } = require('./helpers')
const { register, login, forgotPassword } = require('../controllers/authController')

const router = express.Router();

router.post('/register', asyncWrapper(register));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = router;