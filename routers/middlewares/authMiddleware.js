const jwt = require('jsonwebtoken');

module.exports.authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res
        .status(401)
        .json({message: `No Authorization http header found!`});
  }

  const token = header.split(' ')[1];

  if (!token) {
    return res.status(401).json({message: `No JWT token found!`});
  }

  req.user = jwt.verify(token, process.env.JWT_SECRET);
  next();
};

module.exports.isAuthorized = (req, res, next) => {
  const header = req.headers['authorization'];
  let authorized = false;
  if (header) {
    const [tokenType, token] = header.split(' ');
    if (token) {
      jwt.verify(token, JWT_SECRET, (err, dec) => {
        if (!err) {
          authorized = true;
          req.user = dec;
        }
      });
    }
  }
  req.authorized = authorized;
  next();
};
