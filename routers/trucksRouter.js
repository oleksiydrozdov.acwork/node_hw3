const express = require("express");
const {
  getTrucks,
  addTruck,
  deleteTruck,
  assignTruck,
  getTruck,
  updateTruck,
} = require("../controllers/trucksController");
const { asyncWrapper } = require("./helpers");
const { authMiddleware } = require("./middlewares/authMiddleware");

const router = express.Router();

router.use('*', authMiddleware)
router.get("/", asyncWrapper(getTrucks));
router.post("/", asyncWrapper(addTruck));
router.get("/:id", asyncWrapper(getTruck));
router.put("/:id", asyncWrapper(updateTruck));
router.delete("/:id", asyncWrapper(deleteTruck));
router.post("/:id/assign", asyncWrapper(assignTruck));

module.exports = router;
